FROM gitpod/workspace-full-vnc
USER gitpod

RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal main restricted universe multiverse' > /etc/apt/sources.list"
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal-security main restricted universe multiverse' >> /etc/apt/sources.list"
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal-updates main restricted universe multiverse' >> /etc/apt/sources.list"
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal-proposed main restricted universe multiverse' >> /etc/apt/sources.list"
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal-backports main restricted universe multiverse' >> /etc/apt/sources.list"
RUN sudo apt-get -yq update

RUN sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install gdebi p7zip-full unar unzip zip wget openbox lxpanel pcmanfm file-roller deluge firefox handbrake

RUN sudo sh -c "wget http://170.210.201.179/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_80.0.3987.87-1_amd64.deb -O google-chrome_amd64.deb"
RUN sudo gdebi -n 'google-chrome_amd64.deb'
RUN sudo sh -c "apt-get clean"

RUN sudo sh -c "rm /usr/share/applications/google-chrome.desktop"

USER root
COPY google-chrome.desktop.conf /usr/share/applications/google-chrome.desktop
USER gitpod

RUN sudo sh -c "chmod 0755 /usr/share/applications/google-chrome.desktop"

ENV DISPLAY :0
RUN sudo sh -c "Xvfb -screen 0 856x480x16 -ac > /dev/null 2>&1" &
